## Peer Graded Assignment: Classe que manipula usuários no banco de dados

#### Instructions

O objetivo dessa aula é exercitar a criação de classes que utilizam JDBC para acessar o banco de dados.

Primeiramente deve ser criado o banco de dados chamado "coursera" e criada a tabela usuario.

Deve ser criada uma classe Usuario com as informações presentes na tabela e uma interface.

Como o foco do exercício não é a criação de consultas.

Como exercício, o aluno deverá implementar uma classe que implementa a interface apresentada fazendo acesso ao banco de   
dados utilizando JDBC. Outros frameworks não devem ser utilizados.

Cada um dos métodos acima deve ser testado utilizando o DBUnit.

Deve ser entregue um projeto (no Eclipse ou Netbeans) com a classe e os testes pedidos em um arquivo do tipo .zip  
e uma imagem da tela que mostra o resultado da execução dos testes.

#### Review criteria 

Será considerado na avaliação:

* Se o que foi pedido foi implementado.
* Se os requisitos funcionais foram atendidos
* Se a implementação utilizou as tecnologias solicitadas
* Se os testes foram criados como solicitado